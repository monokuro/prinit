# Prinit - Prettier-init

A small CLI tool for quickly and easily creating prettier config files (`prettierrc`)

## Install

```bash
npm install -g @whiite/prinit
```

## Usage

-   `prinit` - Creates a `prettierrc` JSON file using the saved config
-   `prinit use <file>` - Sets a desired `prettierrc` to be used as the default next time `prinit` is run
    -   _File is saved to `$HOME/.prinit/user-config.json`_
-   `prinit default` - Creates a `prettierrc` with all defaults
-   `prinit --json` - Creates a `prettierrc` file from the saved config in JSON format (_the same as passing no argument_)
-   `prinit --yaml` - Creates a `prettierrc` file from the saved config in YAML format
-   `prinit --toml` - Creates a `prettierrc` file from the saved config in TOML format (_the same as passing no argument_)
-   `prinit --minimum` - Creates a `prettierrc` file from the saved config with default options excluded
-   `prinit --replace` - Replaces all existing `prettierrc` files with the config created from a command

Example:

```bash
prinit default # Creates the default prettierrc

# Edit prettierrc file with your desired settings...
# Saves the found prettier config as the default
prinit use

# Alternatively specify the config file to use as the default
prinit use .prettierrc.json

# In a new project use:
prinit --minimum --yaml # Creates prettierrc using the saved file, default options excluded
```

### Usage notes:

-   Specifying more than one format option (`--yaml`, `--json`) will use the format with the highest priority:
    -   **Priority order:** _JSON > YAML > TOML_.
-   Specifying no format type will default to JSON
-   If no file is specified with the `use` command, the prettier file will be searched for starting in the current directory and looking upwards
-   If more than one `.prettierrc` file is found in a directory the file with the highest priority according to the order used by [cosmiconfig](https://github.com/davidtheclark/cosmiconfig)
