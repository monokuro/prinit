import { readFile, writeFile, mkdir, readdir } from 'fs/promises'
import { join } from 'path'
import { homedir } from 'os'
import { DefaultConfig } from './default-config'
import yaml from 'js-yaml'
import { resolveConfigFile } from 'prettier'
import { FormatType } from './cli-utils'
import { cosmiconfig } from 'cosmiconfig'
import TOML, { JsonMap } from '@iarna/toml'

export const PRETTIER_CONFIG_FILE_NAME = '.prettierrc'
export const SETTINGS_PATH = join(homedir(), '.prinit')
export const USER_CONFIG_PATH = join(SETTINGS_PATH, 'user-config.json')

// Cosmiconfig loader
const explorer = cosmiconfig('prettier')

/**
 * Loads a config file previously saved by the user
 * @returns The parsed saved config file
 */
export async function loadUserConfig(): Promise<any> {
	const config = await readFile(USER_CONFIG_PATH, 'utf-8')
	return JSON.parse(config)
}

/** Writes a config to the 'prinit' settings folder */
export async function setUserConfig(filepath?: string) {
	let userConfig: object | null

	// Automatically find the relevant config
	if (!filepath) filepath = (await resolveConfigFile(process.cwd())) ?? ''

	if (!filepath) throw 'No config file found'
	const loadedConfig = await explorer.load(filepath).catch((err) => {
		throw `Unable to load config: ${err}`
	})

	userConfig = loadedConfig?.config
	if (!userConfig) throw 'Config was empty'

	await mkdir(SETTINGS_PATH, { recursive: true })
	await writeFile(USER_CONFIG_PATH, JSON.stringify(userConfig, undefined, 2))
}

/** Returns the saved user config with all prettier defaults excluded */
export async function getMinimumUserConfig() {
	console.log('Getting minimal')
	const userConfig = await loadUserConfig()
	const minimumConfig: any = {}

	// Run through properties extracting non-default options
	Object.keys(userConfig).forEach((option) => {
		if (
			userConfig[option] ===
			DefaultConfig[option as keyof typeof DefaultConfig]
		)
			return

		// Different option from default
		minimumConfig[option] = userConfig[option]
	})
	return minimumConfig
}

/**
 * Checks for existing config files and returns found paths
 *
 * @param path Directory path to check
 */
export async function existingConfig(path: string = '.') {
	return (await readdir(path)).filter((pathLike) =>
		pathLike.startsWith(PRETTIER_CONFIG_FILE_NAME),
	)
}

/**
 * Converts a JSON config object to a supported file type
 * @param config JSON config object to convert to a file
 * @param fileType The file type to convert the config to
 * @returns Converted config file string
 */
export function convertConfigToFile(
	config: string | object,
	fileType: FormatType,
): string {
	if (typeof config === 'string') config = JSON.parse(config) as object

	let configFileFormat: string
	switch (fileType) {
		case FormatType.JSON:
			configFileFormat = JSON.stringify(config, undefined, 4)
			break
		case FormatType.YAML:
			configFileFormat = yaml.dump(config)
			break
		case FormatType.TOML:
			configFileFormat = TOML.stringify(config as JsonMap)
			break
	}

	return configFileFormat
}
