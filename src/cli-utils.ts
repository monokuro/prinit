import readline from 'readline'

/** Supported file types for writing */
export enum FormatType {
	JSON = 'json',
	YAML = 'yaml',
	TOML = 'toml',
}

type FormatOptions = {
	[key in `${FormatType}`]?: boolean
}

/**
 * Returns the first file type found in order of precedence (@see FormatType)
 * Defaults to 'json' if none are found
 * @param options Program options
 * @returns The found format type
 */
export function resolveFileType(options: FormatOptions): FormatType {
	for (const formatOption of Object.values(FormatType)) {
		if (options[formatOption]) return formatOption
	}

	// Default to JSON when nothing is found
	return FormatType.JSON
}

/**
 * Prompts the user a given question and waits for a `y/n` response
 *
 * @param question The question to prompt the user with
 */
export async function questionUser(question: string): Promise<boolean> {
	// User input handler
	const rl = readline.createInterface({
		input: process.stdin,
		output: process.stdout,
	})

	let answeredYes = false
	return new Promise((resolve, reject) => {
		rl.question(`${question} (Y/n) `, async (answer) => {
			answer = answer.toLowerCase()
			if (!answer || answer == 'yes' || answer == 'y') answeredYes = true
			rl.close()
			resolve(answeredYes)
		})
	})
}
