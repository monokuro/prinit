import { writeFile, rm } from 'fs/promises'
import { Command } from 'commander'
import chalk from 'chalk'

import { DefaultConfig } from './default-config'
const { version: packageVersion } = require('../package.json')
import {
	setUserConfig,
	loadUserConfig,
	getMinimumUserConfig,
	USER_CONFIG_PATH,
	PRETTIER_CONFIG_FILE_NAME,
	convertConfigToFile,
	existingConfig,
} from './config-tools'
import { questionUser, resolveFileType } from './cli-utils'

export const program = new Command()

// Options
program
	.version(packageVersion)
	.description('Generate and save prettier config files quickly and easily')
	.option('-j, --json', "Use the JSON style 'pretterrc' file format")
	.option('-y, --yaml', "Use the YAML style 'pretterrc' file format")
	.option('-t, --toml', "Use the TOML style 'pretterrc' file format")
	.option(
		'-m, --minimum',
		'Use the set config with prettier defaults excluded',
	)
	.option('-r, --replace', 'Replace existing config files')

// No subcommand - Normal usage
program
	.command('init', {
		isDefault: true,
	})
	.description(
		"Generates a 'prettierrc' file using the set config. [Default command]",
	)
	.action(async () => {
		const options = program.opts()
		try {
			const userConfig = options.minimum
				? await getMinimumUserConfig()
				: await loadUserConfig()
			await writeConfig(userConfig)
		} catch {
			console.error(chalk.red('No config set'))
			console.log(
				'Use',
				chalk.yellow('prinit use <file>'),
				'to set a config first',
			)

			if (await questionUser('Do you want to use the prettier defaults?'))
				await writeConfig(DefaultConfig, 'default')
		}
	})

// Generate the default config file
program
	.command('default')
	.description("Create a 'prettierrc' file with all defaults")
	.action(async () => {
		// Check if config files already exists
		if (program.opts().replace)
			for (const configPath of await existingConfig())
				await rm(configPath)
		await writeConfig(DefaultConfig, 'default')
	})

// Sets a prettierrc file as the users default
program
	.command('use [file]')
	.alias('set')
	.description(
		"Sets a 'prettierrc' file to be used as the default config with 'prinit' or finds it automatically if no file is specified",
	)
	.action(async (filepath?: string) => {
		try {
			await setUserConfig(filepath)
			console.log(chalk.green('Set prettier config'))
			console.log(
				chalk.green(
					`Config saved to: ${chalk.yellow(`'${USER_CONFIG_PATH}'`)}`,
				),
			)
			return
		} catch (err) {
			console.error(chalk.red(err))
		}
	})

/** Writes a config file to the current directory */
async function writeConfig(config: string | object, configLabel?: string) {
	const options = program.opts()

	// Get the desired file type
	const fileType = resolveFileType(options)

	// Check if config files already exists
	if (options.replace)
		for (const configPath of await existingConfig()) await rm(configPath)

	// Setup the config file
	const configFile = {
		name: `${PRETTIER_CONFIG_FILE_NAME}.${fileType}`,
		contents: convertConfigToFile(config, fileType),
	}

	// Write the file
	await writeFile(configFile.name, configFile.contents, 'utf-8')
	console.log(
		chalk.green(
			`Generated${configLabel ? ` ${configLabel}` : ''}`,
			chalk.yellow(`'${configFile.name}'`),
		),
	)
}

/** Runs the CLI program */
export default async function main() {
	await program.parseAsync(process.argv)
}

if (require.main === module) main()
