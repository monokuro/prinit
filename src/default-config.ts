/** Prettier config with all default settings applied */
export const DefaultConfig = {
	printWidth: 80,
	tabWidth: 2,
	useTabs: false,
	semi: true,
	singleQuote: false,
	quoteProps: 'as-needed',
	jsxSingleQuote: false,
	trailingComma: 'es5',
	bracketSpacing: true,
	jsxBracketSameLine: true,
	arrowParens: 'always',
	rangeStart: 0,
	// rangeEnd: Infinity, // NOTE: Doesn't convert 'Infinity' to JSON
	// parser: null, // NOTE: No value means auto
	// filepath: null, // Not applicable in config files
	requirePragma: false,
	insertPragma: false,
	proseWrap: 'preserve',
	htmlWhitespaceSensitivity: 'css',
	vueIndentScriptAndStyle: false,
	endOfLine: 'lf',
	embeddedLanguageFormatting: 'auto',
}
